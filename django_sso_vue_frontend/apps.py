from django.apps import AppConfig


class DjangoSsoVueFrontendConfig(AppConfig):
    name = 'django_sso_vue_frontend'
