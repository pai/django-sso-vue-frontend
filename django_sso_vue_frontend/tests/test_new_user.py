import time
import os
import re
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from django.core import mail

from .base import TestFunzionale


class NuovoVisitatoreTest(TestFunzionale):
       
    def test_puo_registrarsi_al_sito(self):
        step = 0
        # L'utente si connette all'indirizzo della pagina
        app_url = self.server_url + '?next=' + self.service.url
        self.browser.get(app_url)
        time.sleep(6)
        
        # riconosce la pagina
        self.identifica_titolo('Sign In')
        
        # che gli chiede di inserire le credenziali di accesso
        self.tag_contiene_testo('h3', 'Insert username or e-mail. If you have not created an account yet, then please SIGN UP first.')

        # prova a loggarsi
        self.compila_input_by_name('login', 'pippo@disney.com')
        time.sleep(1)
        
        # vede che non è possibile senza creare un nuovo utente
        self.tag_contiene_testo('h3', 'Account not found, please SIGN UP.')
        
        # segue il link di registrazione
        self.click_link('/signup/?next='+self.service.url)
        time.sleep(1)
        
        # inserisce email sbagliata
        self.compila_input_by_name('email', 'pippo@disney.')
        time.sleep(1)
        
        # Legge messaggio di errore validazione email
        self.tag_contiene_testo('h3', 'Check e-mail.')
        
        # clicca sul pulsante pulisci campo
        clearbtn = self.find_by_tag_class_and_text('i', 'icon', 'clear')
        clearbtn.click()
        time.sleep(1)
        
        # inserisce email giusta
        self.compila_input_by_name('email', 'pippo@disney.com')
        time.sleep(1)
        step += 1
        
        # vede che gli viene richesto un nome utente
        self.tag_contiene_testo('h3', 'Choose username.')
        
        # inserisce username
        self.compila_input_by_name('username', 'pippo')
        time.sleep(1)
        step += 1
        
        # vede che gli viene richesta una password
        self.tag_contiene_testo('h3', 'Choose a strong password.')
        
        # inserisce password corta
        self.compila_input_by_name('password1', 'pippa')
        time.sleep(1)
        
        # vede che gli viene richesta una password piu strong
        self.tag_contiene_testo('h3', 'Password must be at least 8 characters long.')
        
        # inserisce password corta
        self.compila_input_by_name('password1', 'paperina')
        time.sleep(1)
        
        # vede che gli viene richesta nuovamente la password
        self.tag_contiene_testo('h3', 'Confirm password.')
        
        # inserisce password
        self.compila_input_by_name('password2', 'paperinia')
        time.sleep(1)
        
        # vede che ha sbagliato a digitare
        self.tag_contiene_testo('h3', "Password didn't match.")
        
        # inserisce password
        self.compila_input_by_name('password1', 'paperina')
        time.sleep(1)
        step += 1
        
        # conferma password
        self.compila_input_by_name('password2', 'paperina')
        time.sleep(1)
        step += 1
        
        # Gli si chiede di inserire il nome
        self.tag_contiene_testo('h3', "Insert first name.")
        
        # mette nome
        self.compila_input_by_name('first_name', 'Pippo')
        time.sleep(1)
        step += 1
        
        # Gli si chiede di inserire il cognome
        self.tag_contiene_testo('h3', "Insert last name.")
        
        # mette cognome
        self.compila_input_by_name('last_name', 'Goofy')
        time.sleep(1)
        step += 1
        
        # Gli si chiede di parlare di se
        self.tag_contiene_testo('h3', "About you..")
        
        # mette description
        self.compila_input_by_name('description', 'Yuk! Yuk yuk yuk yuk!! YUK!', el="textarea")
        time.sleep(1)

        # clicca sul pulsante avanti
        self.press_next_button(step)
        time.sleep(1)
        step += 1

        # Gli si chiede di inserire la posizione
        self.tag_contiene_testo('h3', "Set your position.")
        
        # inserisce posizione
        campo = self.compila_input_by_name('address', 'viale ettore andreis 74 desenzano', False)
        time.sleep(4)
        campo.send_keys(Keys.ARROW_DOWN)
        time.sleep(1)
        campo.send_keys(Keys.ENTER)
        time.sleep(1)
        
        # clicca sul pulsante avanti
        self.press_next_button(step)
        step += 1
        time.sleep(1)
        
        # inserisce immagine
        avatar_path = os.path.join(os.getcwd(), 'backend', 'tests', 'assets', 'avatar.png')
        self.browser.find_element_by_name('picture').send_keys(avatar_path)
        
        # clicca sul pulsante avanti
        self.press_next_button(step)
        step += 1
        time.sleep(1)
        
        # Gli si chiede di leggere la licenza
        self.tag_contiene_testo('h3', "Clink CONTINUE to accept terms of service.")
        
        # accetta la licenza
        self.press_next_button(step)
        step += 1
        time.sleep(1)
        
        # Lo si informa dell'avvenuta registrazione
        self.tag_contiene_testo('h3', "Confirmation e-mail sent. Please check your inbox.")
        time.sleep(1)
        
        # Controlla la casella della posta in arrivo e segue il link di attivazione
        self.assertEqual(len(mail.outbox), 1)
        message = str(mail.outbox[0].message())
        activate_email_url = re.search("(?P<url>https?://.[^\s]+)", message).group("url")
        self.browser.get(activate_email_url)
        
        time.sleep(3)
        
        # il link di attivazione lo redirige alla pagina di login
        self.tag_contiene_testo('h3', 'Insert username or e-mail. If you have not created an account yet, then please SIGN UP first.')
        
        # inserise le credenziali
        self.compila_input_by_name('login', 'pippo@disney.com')
        self.compila_input_by_name('password', 'paperina')
        
        
        print('\n\nEOT')
        time.sleep(3)
