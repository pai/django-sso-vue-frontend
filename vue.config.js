module.exports = {
  lintOnSave: true,
  
  // mine 1
  chainWebpack: config => {
    config.externals({
      'vue': 'Vue',
      'axios': 'axios',
      'vue-custom-element': 'VueCustomElement',
      'swiper': 'Swiper'
    })
    // https://forum.vuejs.org/t/disable-code-splitting-in-vue-cli-3/36295/3
    //config.optimization.delete('splitChunks')
    //config.optimization.splitChunks(false)
    
    // production html template
    config.plugin("html").tap(args => {
      if (process.env.NODE_ENV === 'production') {
        args[0].template = "./src/html-webpack-template.html"
        args[0].inject = false
        args[0].minify = {}
      }
      return args
    })
  },
  baseUrl: process.env.NODE_ENV === 'production'
    ? '/static/'
    : '/',
  css: { loaderOptions: { sass: { includePaths: ['./node_modules'] } } },
  outputDir: './django_sso_vue_frontend/static',
  assetsDir: 'frontend',
  indexPath: '../templates/frontend/built.html',
}
