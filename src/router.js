import Vue from 'vue'
import Router from 'vue-router'

import LoginView from './components/views/LoginView'
import RegisterView from './components/views/RegisterView'
import SendPasswordResetView from './components/views/SendPasswordResetView'
import PasswordResetView from './components/views/PasswordResetView'
import UserProfileUpdateView from './components/views/UserProfileUpdateView'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    { path: '/', redirect: '/login/' },
    { path: '/email/', redirect: '/profile/update/' },
    {
      path: '/login/',
      name: 'Login',
      component: LoginView,
      props: true
    },
    {
      path: '/signup/',
      name: 'Register',
      component: RegisterView,
      props: true
    },
    {
      path: '/password/reset/',
      name: 'Send password reset',
      component: SendPasswordResetView,
      props: true
    },
    {
      path: '/password/reset/confirm/:uid?/:token?/',
      name: 'Reset password',
      component: PasswordResetView,
      props: true
    },
    {
      path: '/profile/update/',
      name: 'Update profile',
      component: UserProfileUpdateView,
      props: true
    }
  ]
})

