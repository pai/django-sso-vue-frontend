export default {
  props: {
    mapCenter: {
      type: Object,
      default () {
        return {
          lat: 45.4687175,
          lng: 10.527247999999986
        }
      }
    },
    mapZoom: {
      type: Number,
      default: 6
    }
  },
  data () {
    return {
      googleMaps: null,
      googleMap: null
    }
  },
  mounted () {
    this.$googleMapsLoaded().then((ev) => {
      this.googleMaps = ev
    }, (err) => {
      throw 'GoogleMapsError'
    })
  },
  methods: {
    googleMapsLoaded (maps) {
      // console.log('GoogleMaps loaded', maps)
    }
  },
  watch: {
    googleMaps (n, o) {
      if (n) {
        this.googleMapsLoaded(n)
      }
    }
  }
}
