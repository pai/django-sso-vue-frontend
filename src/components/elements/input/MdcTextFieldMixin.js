import {MDCTextField, MDCTextFieldIcon} from '@material/textfield/dist/mdc.textfield'
// import {MDCTextField} from '@material/textfield'
// import {MDCTextFieldIcon} from '@material/textfield/icon'

export default {
  props: {
    label: {
      type: String,
      default: ''
    },
    placeholder: {
      type: String,
      default: ''
    },
    type: {
      type: String,
      default: 'text'
    },
    name: {
      type: String,
      default: null
    },
    disabled: {
      type: Boolean,
      default: false
    },
    required: {
      type: Boolean,
      default: false
    },
    minlength: {
      type: [String, Number],
      default: 0
    },
    mdcIcon: {
      type: String,
      default: ''
    },
    tabindex: {
      type: [String, Number],
      default: -1
    }
  },
  data () {
    return {
      value: null,
      textField: null,
      textFieldIcon: null,
      valid: true
    }
  },
  computed: {
    icon () {
      if (this.value && this.value !== '') {
        return 'clear'
      } else {
        return this.mdcIcon
      }
    }
  },
  mounted () {
    this.textField = new MDCTextField(this.$refs.inputContainer)
    if (this.$refs.icon) {
      this.textFieldIcon = new MDCTextFieldIcon(this.$refs.icon)
    }
  },
  methods: {
    clear () {
      // console.log('CLEARING', this.name)
      this.value = null
      this.$refs.input.value = null
      this.$refs.input.focus()
      this.$emit('input', null)
    },
    focus () {
      this.$refs.input.focus()
    },
    blur () {
      this.$refs.input.blur()
    },
    nextTabindex (i) {
      return i
    }
  },
  watch: {
    value (n) {
      this.valid = this.textField.foundation_.isValid()
      this.$emit('input', n)
    }
  }
}
