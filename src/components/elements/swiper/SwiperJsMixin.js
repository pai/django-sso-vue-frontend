import Swiper from 'swiper'

export default {
  props: {
    slidesPerView: {
      type: Number,
      default: 1
    },
    spaceBetween: {
      type: Number,
      default: 0
    },
    autoHeight: {
      type: Boolean,
      default: false
    },
    swipeable: {
      type: Boolean,
      default: true
    },
    allowSlideNext: {
      type: Boolean,
      default: true
    },
    allowSlidePrev: {
      type: Boolean,
      default: true
    },
    direction: {
      type: String,
      default: 'horizontal'
    },
    centeredSlides: {
      type: Boolean,
      default: true
    }
  },
  data () {
    return {
      swiper: null,
      swiperId: null,
      activeStep: 0
    }
  },
  created () {
    this.swiperId = '__swiper__' + Math.random().toString(36).substring(7)
  },
  mounted () {
    this.swiper = new Swiper('#' + this.swiperId, {
      direction: this.direction,
      slidesPerView: this.slidesPerView,
      spaceBetween: this.spaceBetween,
      autoHeight: this.autoHeight,
      swipeable: this.swipeable,
      allowSlidePrev: this.allowSlidePrev,
      allowSlideNext: this.allowSlideNext,
      centeredSlides: this.centeredSlides
    })

    this.swiper.on('slideChange', () => {
      this.$emit('slideChanging', this.swiper.activeIndex)
    })
    this.swiper.on('transitionEnd', () => {
      this.$emit('slideChanged', this.swiper.activeIndex)
    })
    // console.log('SwiperJs mounted', this.swiper, this.swipeable)
  }
}
