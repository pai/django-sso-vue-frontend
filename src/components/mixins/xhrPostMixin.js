export default {
  data () {
    return {
      xhrPostMaxRetries: 0,
      xhrPostRetries: 0,
      xhrPostParams: {},
      xhrPostOptions: {},
      xhrPostUrl: null
    }
  },
  mounted () {
    this.$on('xhrPostError', (error) => {
      if (this.xhrPostRetries < this.xhrPostMaxRetries) {
        //console.log('xhrPostError', error)
        this.xhrPostRetries += 1
        this.performXhrPost()
      } else {
        //console.log('xhrPost Maximum retries reached.')
      }
    })
  },
  methods: {
    performXhrPost () {
      this.$emit('performingXhrPost', this.name)
      this.$http.post(
        this.xhrPostUrl,
        this.xhrPostParams,
        this.xhrPostOptions
      ).then((response) => {
        this.$emit('xhrPostResponse', response)
      }).catch((error) => {
        this.$emit('xhrPostError', error)
      })
    }
  }
}
