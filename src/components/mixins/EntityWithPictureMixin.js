export default {
  computed: {
    pictureStyle () {
      let style = {}
      if (this.entity) {
        if (this.entity.picture) {
          style['background-image'] = 'url(\'' + this.entity.picture + '\')'
        }
      }
      return style
    }
  }
}
