export default {
  props: {
    stepOperation: {
      type: Object,
      default: null
    },
    step: {
      type: String,
      default: null
    },
    description: {
      type: String,
      default: null
    },
    remoteStep: {
      type: Boolean,
      default: false
    },
    firstStep: {
      type: Boolean,
      default: false
    },
    lastStep: {
      type: Boolean,
      default: false
    }
  },
  data () {
    return {
      result: null,
      keyIndex: {}
    }
  },
  computed: {
    steps () {
      return this.stepOperation.steps.length
    },
    active () {
      return this.step === this.stepOperation.activeStep
    }
  },
  created () {
    for (let i = 0; i < this.stepOperation.steps.length; i++) {
      let step = this.stepOperation.steps[i]
      this.keyIndex[step.key] = i
    }
  },
  methods: {
    stepActivated () {
      this.$emit('stepActivated', this.step)
    }
  },
  watch: {
    'stepOperation.activeStep': function (n) {
      if (this.step === n) {
        this.stepActivated()
      }
    }
  }
}
