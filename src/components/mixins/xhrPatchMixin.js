export default {
  data () {
    return {
      xhrPatchMaxRetries: 0,
      xhrPatchRetries: 0,
      xhrPatchParams: {},
      xhrPatchOptions: {},
      xhrPatchUrl: null
    }
  },
  mounted () {
    this.$on('xhrPatchError', (error) => {
      if (this.xhrPatchRetries < this.xhrPatchMaxRetries) {
        console.log('xhrPatchError', error)
        this.xhrPatchRetries += 1
        this.performXhrPatch()
      } else {
        console.log('xhrPatch Maximum retries reached.')
      }
    })
  },
  methods: {
    performXhrPatch () {
      this.$http.patch(
        this.xhrPatchUrl,
        this.xhrPatchParams,
        this.xhrPatchOptions
      ).then((response) => {
        this.$emit('xhrPatchResponse', response)
      }).catch((error) => {
        this.$emit('xhrPatchError', error)
      })
    }
  }
}
