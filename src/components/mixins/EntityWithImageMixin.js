export default {
  computed: {
    imageStyle () {
      let style = {}
      if (this.entity) {
        if (this.entity.image) {
          style['background-image'] = 'url(\'' + this.entity.image + '\')'
        }
      }
      return style
    }
  }
}
