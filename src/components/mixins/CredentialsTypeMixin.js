/*
Check if login is either by username or by email.
*/

import FieldValidatorsMixin from './FieldValidatorsMixin'

export default {
  mixins: [
    FieldValidatorsMixin
  ],
  methods: {
    getCredentialsType (string) {
      // console.log('VALUTO CREDENZIALI', string)
      if (this.validateEmail(string)) {
        return 'email'
      } else if (this.validateUsername(string)) {
        return 'username'
      } else {
        return null
      }
    },
    credentialsTypeIsValid (string) {
      let loginType = this.getCredentialsType(string)
      // console.log('ho trovato in tipo', string, loginType)
      if (loginType === 'email' || loginType === 'username') {
        return true
      }
      return false
    }
  }
}
