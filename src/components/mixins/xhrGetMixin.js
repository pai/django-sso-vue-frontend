export default {
  data () {
    return {
      xhrGetMaxRetries: 0,
      xhrGetRetries: 0,
      xhrGetParams: {},
      xhrGetOptions: {},
      xhrGetUrl: null
    }
  },
  mounted () {
    this.$on('xhrGetError', (error) => {
      if (this.xhrGetRetries < this.xhrGetMaxRetries) {
        console.log('xhrGetError', error)
        this.xhrGetRetries += 1
        this.performXhrGet()
      } else {
        console.log('xhrGet Maximum retries reached.')
      }
    })
  },
  methods: {
    performXhrGet () {
      this.$emit('performingXhrGet', this.name)
      this.$http.get(
        this.xhrGetUrl,
        this.xhrGetParams,
        this.xhrGetOptions
      ).then((response) => {
        this.$emit('xhrGetResponse', response)
      }).catch((error) => {
        this.$emit('xhrGetError', error)
      })
    }
  }
}
