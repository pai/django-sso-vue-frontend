let gettext = window.gettext || function (s) { return s }
let genericError = gettext('Something happened in setting up the request that triggered an Error. Please contact support.')
let serverUnreacheableError = gettext('Server is unreacheable, please check your connection or try later.')

export default {
  methods: {
    parseError: function (error) {
      // console.log('Parsing Error Response', error)

      if (error.response) {
        let response = error.response.data
        let message = ''

        if (response) {
          if (typeof response === 'object') {
            let i = 0
            for (let property in response) {
              i += 1
              if (response.hasOwnProperty(property)) {
                message += response[property]
              }
              if (i > 1) {
                message = ', ' + message
              }
              return message
            }
          } else if (typeof response === 'string') {
            return response
          } else {
            return genericError
          }
        } else {
          return genericError
        }
      } else if (error.request) {
        return serverUnreacheableError
      } else {
        return genericError
      }
    }
  }
}
