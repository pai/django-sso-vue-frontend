export default {
  computed: {
    logoStyle () {
      let style = {}
      if (this.entity) {
        if (this.entity.logo) {
          style['background-image'] = 'url(\'' + this.entity.logo + '\')'
        }
      }
      return style
    }
  }
}
