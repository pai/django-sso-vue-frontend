export default {
  data () {
    return {
      browserType: null
    }
  },
  created () {
    this.browserType = this.getBrowserType()
  },
  methods: {
    getBrowserType () {
      // Opera 8.0+
      let isOpera = (!!window.opr && !!window.opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0

      // Firefox 1.0+
      let isFirefox = typeof InstallTrigger !== 'undefined'

      // Safari 3.0+ '[object HTMLElementConstructor]'
      let isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === '[object SafariRemoteNotification]' })(!window['safari'] || (typeof window.safari !== 'undefined' && window.safari.pushNotification))

      // Internet Explorer 6-11
      let isIE = false || !!document.documentMode // @cc_on!@

      // Edge 20+
      let isEdge = !isIE && !!window.StyleMedia

      // Chrome 1+
      let isChrome = !!window.chrome && !!window.chrome.webstore

      // Blink engine detection
      let isBlink = (isChrome || isOpera) && !!window.CSS

      return {
        opera: isOpera,
        firefox: isFirefox,
        safari: isSafari,
        ie: isIE,
        edge: isEdge,
        chrome: isChrome,
        blink: isBlink
      }
    }
  }
}
