export default {
  props: {
    loaded: {
      type: Boolean,
      default: false
    },
    loadingText: {
      type: String,
      default: '..'
    }
  },
  data () {
    return {
      loading: false
    }
  },
  watch: {
    loaded (n) {
      this.loading = !n
    }
  },
  methods: {
    stopLoading () {
      this.loading = false
    },
    startLoading () {
      this.loading = true
    }
  }
}
