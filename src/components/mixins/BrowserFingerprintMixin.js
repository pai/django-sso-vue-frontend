/*
 Calculate Browser Fingerprint
*/

import Fingerprint from 'fingerprintjs2'

export default {
  props: {
    fingerprintOptions: {
      type: Object,
      default () {
        return {}
      }
    }
  },
  data () {
    return {
      fingerprintjs2: null,
      browserFingerprint: null
    }
  },
  created () {
    this.browserFingerprint = 'undefined'
    this.fingerprintjs2 = new Fingerprint(this.fingerprintOptions).get((result, components) => {
      if (result) {
        // console.log('FP res', result) // a hash, representing your device fingerprint
        // console.log('FP comp', components) // an array of FP components
        this.browserFingerprint = result
      }
    })
  }
}
