export default {
  data () {
    return {
      servicesUrl: '/api/v1/services/',
      services: null
    }
  },
  methods: {
    forwardToService (service) {
      // console.log('forwarding to service', service)
      window.location.href = service.url
    },
    getServicesList () {
      this.$http.get(
        this.servicesUrl,
        {}
      ).then((response) => {
        this.services = response.data
        this.$emit('servicesFetched', this.services)
      }).catch((error) => {
        this.$emit('errorFetchingServices', error)
      })
    },
    subscribeToService (service) {
      // console.log('subscribing to service', service)
      let url = this.servicesUrl + service.id + '/subscribe/'
      this.$http.post(
        url,
        {}
      ).then(() => {
        this.$emit('serviceSubscribed', service)
      }).catch((error) => {
        this.$emit('errorSubscribingService', error)
      })
    }
  }
}
