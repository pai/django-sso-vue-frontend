export default {
  props: {
    userProfilesUrl: {
      type: String,
      default: '/api/v1/profiles/users/'
    }
  },
  data () {
    return {
      userProfile: null
    }
  },
  methods: {
    patchUserProfile (obj) {
      let url = this.userProfilesUrl + this.userProfile.id + '/'
      this.$http.patch(
        url,
        obj
      ).then((response) => {
        this.$emit('userProfilePatched', response)
      }).catch((error) => {
        this.$emit('errorPatchingUserProfile', error)
      })
    }
  }
}
