export default {
  props: {
    userProfileUrl: {
      type: String,
      default: '/api/v1/auth/user/'
    }
  },
  data () {
    return {
      userProfile: null
    }
  },
  methods: {
    fetchUserProfile () {
      this.$http.get(
        this.userProfileUrl,
        {}
      ).then((response) => {
        this.userProfile = response.data
        this.$emit('userProfileFetched', this.userProfile)
      }).catch((error) => {
        this.$emit('errorFetchingUserProfile', error)
      })
    }
  }
}
