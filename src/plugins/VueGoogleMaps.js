/*
  VueJs Google Maps Plugin
*/

const getGoogleMaps = () => {
  return new Promise((resolve, reject) => {
    if (typeof window !== 'undefined' && !window.loadingGoogleMaps && window.google && window.google.maps) {
      resolve(window.google.maps)
    } else {
      document.addEventListener('googleMapsLoaded', (ev) => {
        resolve(ev.detail)
      })
    }
  })
}

const VueGoogleMaps = {
  install (Vue, options) {
    Vue.mixin({
      mounted () {
        Vue.prototype.$googleMapsLoaded = getGoogleMaps
      }
    })
  }
}

// Automatic installation if Vue has been added to the global scope.
if (typeof window !== 'undefined' && window.Vue) {
  window.Vue.use(VueGoogleMaps)
}

export default VueGoogleMaps
