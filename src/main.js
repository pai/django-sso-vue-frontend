import Vue from 'vue'

import axios from 'axios'

import VueGoogleMaps from './plugins/VueGoogleMaps'

import router from './router'
import App from './App'

axios.defaults.xsrfCookieName = 'csrftoken'
axios.defaults.xsrfHeaderName = 'X-CSRFToken'


// Vue.use(VueGoogleMaps, {
//   load: {
//     key: 'AIzaSyBTWOm6kvTJC1SfKp41LjXYqyiiP0iKpJY',
//     libraries: 'places'
//   }
// })

Vue.use(VueGoogleMaps)

Vue.config.productionTip = false
Vue.prototype.$http = axios

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {
    App
  },
  router,
  // store,
  template: '<App/>'
})
