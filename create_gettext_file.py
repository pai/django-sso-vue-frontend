#!/usr/bin/python
import glob, os
import re


REGEXP = re.compile('gettext\(.*?\)')
APP_FOLDER = 'django_sso_vue_frontend'
OUTPUT_FILE = os.path.join(APP_FOLDER, 'static', 'gettext_strings.js')

def find_gettext(s):
    return re.findall(REGEXP, s)

_files = []

for root, dirs, files in os.walk("./src"):
    for file in files:
        if file.endswith(".vue") or file.endswith('js'):
             _files.append(os.path.join(root, file))


_lines = set()

for f in _files:
    with open(f, 'r') as file:
       strings = find_gettext(file.read())
       for s in strings:
            _lines.add(s)


_strings = list(_lines)

with open(OUTPUT_FILE, 'w') as file:
    for s in _strings:
        file.write('{}\n'.format(s))

print('Found', len(_strings), 'different strings, saving to', OUTPUT_FILE)

